//Clasificar ruedas en función de su diámetro
function clasificaRuedaJuguete(diametroRueda) {

    if (diametroRueda <= 0) {
        console.log("Quizá debas revisar la medida introducida.");
    }

    if (diametroRueda > 0 && diametroRueda <= 10) {
        console.log("Es una rueda para un juguete pequeño.");
    }

    if (diametroRueda > 10 && diametroRueda < 20) {
        console.log("Es una rueda para un juguete mediano.");
    }

    if (diametroRueda >= 20) {
        console.log("Es una rueda para un juguete grande.");
    }
}

//corro todos los casos de una 
var arrayRuedas = [-3, 0, 7, 16, 500]; 
var arraySalida;

//var numeroMuestras = length(arrayRuedas);

for (i = 0; i < arrayRuedas.length; i++){
    clasificaRuedaJuguete(arrayRuedas[i]);
}

